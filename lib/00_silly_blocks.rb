def reverser(&prc)
  value = prc.call
  value.split.map do |word|
    word.reverse
  end.join(' ')
end

def adder(num = 1, &prc)
  value = prc.call
  value + num
end

def repeater(times = 1, &prc)
  times.times { prc.call }
end
