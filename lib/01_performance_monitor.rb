def measure(times = 1, &prc)
  runtimes = []
  return if prc.nil?
  times.times do
    start = Time.now
    prc.call
    runtimes << Time.now - start
  end
  runtimes.reduce(:+) / runtimes.length
end
